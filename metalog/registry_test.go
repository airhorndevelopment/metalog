package metalog

import (
	"github.com/sirupsen/logrus"
	"log"
	"reflect"
	"testing"
)

func resetRegistry() {
	registry = map[string]*metalog{"DEFAULT": {}}
	registryLoaded = false
}

func TestGetLoggerWithNoOptions(t *testing.T) {
	l := GetLogger()
	if reflect.ValueOf(l).Type().String() != "*logrus.Logger" {
		log.Fatalf("GetLogger with no options returned unexpeceted value: %s",
			reflect.ValueOf(l).Type().String())
	}
	t.Cleanup(func() {
		resetRegistry()
	})
}

func TestGetLoggerWithRootConfiguration(t *testing.T) {
	config := `
level=debug
`
	l := GetLogger(nil, []byte(config))
	if l.Level != logrus.DebugLevel {
		log.Fatalf("Expected logger to have level %d, got %d", logrus.DebugLevel, l.Level)
	}

	formatterType := reflect.TypeOf(l.Formatter).String()
	if formatterType != "*logrus.TextFormatter" {
		log.Fatalf("Expected logger to have TextFormatter, got %s", formatterType)
	}
	t.Cleanup(func() {
		resetRegistry()
	})
}

func TestGetLoggerWithJsonFormatter(t *testing.T) {
	config := `
level=info
formatter=json
`
	l := GetLogger(nil, []byte(config))
	if l.Level != logrus.InfoLevel {
		log.Fatalf("Expected logger to have level %d, got %d", logrus.InfoLevel, l.Level)
	}

	formatterType := reflect.TypeOf(l.Formatter).String()
	if formatterType != "*logrus.JSONFormatter" {
		log.Fatalf("Expected logger to have JSONFormatter, got %s", formatterType)
	}
	t.Cleanup(func() {
		resetRegistry()
	})
}

func TestNamedLoggerInheritsFromDefault(t *testing.T) {
	config := `
level=debug
formatter=json

[testchild1]
level=info

[testchild2]
formatter=text
`

	l := GetLogger("testchild1", []byte(config))
	if l.Level != logrus.InfoLevel {
		log.Fatalf("Expected logger to have level %d, got %d", logrus.InfoLevel, l.Level)
	}

	formatterType := reflect.TypeOf(l.Formatter).String()
	if formatterType != "*logrus.JSONFormatter" {
		log.Fatalf("Expected logger to have JSONFormatter, got %s", formatterType)
	}

	l = GetLogger("testchild2", []byte(config))
	if l.Level != logrus.DebugLevel {
		log.Fatalf("Expected logger to have level %d, got %d", logrus.DebugLevel, l.Level)
	}

	formatterType = reflect.TypeOf(l.Formatter).String()
	if formatterType != "*logrus.TextFormatter" {
		log.Fatalf("Expected logger to have TextFormatter, got %s", formatterType)
	}
	t.Cleanup(func() {
		resetRegistry()
	})

}