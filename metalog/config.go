package metalog

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"gopkg.in/ini.v1"
	"os"
)

func getConfig(source interface{}) {
	cfg, err := ini.Load(source)
	if err != nil {
		fmt.Println("Failed to load logging configuration, using defaults: " + err.Error())
		registry["DEFAULT"] = &metalog{
			configuration: loggingConfig{
				level:     logrus.InfoLevel,
				formatter: &logrus.TextFormatter{},
				output:    os.Stdout,
			},
		}
		return
	}

	defaultSection, _ := cfg.GetSection("DEFAULT")
	registry["DEFAULT"] = &metalog{
		configuration: loggingConfig{
			level:     parseLevel(defaultSection, nil),
			formatter: parseFormatter(defaultSection, nil),
			output:    os.Stdout,
		},
	}

	for _, section := range cfg.Sections() {
		if section.Name() == "DEFAULT" { continue }
		defaultMetaLoggerCopy := *registry["DEFAULT"]
		defaultMetaLoggerCopy.configuration = loggingConfig{
			level:     parseLevel(section, &defaultMetaLoggerCopy),
			formatter: parseFormatter(section, &defaultMetaLoggerCopy),
			output:    os.Stdout,
		}
		registry[section.Name()] = &defaultMetaLoggerCopy
	}
}

func parseLevel(section *ini.Section, defaultMetalog *metalog) logrus.Level {
	logLevelKey, err := section.GetKey("level")
	if err != nil {
		if defaultMetalog == nil {
			return logrus.InfoLevel
		} else {
			return defaultMetalog.configuration.level
		}
	}

	if mappedLevel, ok := levelMap[logLevelKey.String()]; ok {
		return mappedLevel
	} else {
		return logrus.InfoLevel
	}
}

func parseFormatter(section *ini.Section, defaultMetalog *metalog) logrus.Formatter {
	formatterKey, err := section.GetKey("formatter")
	if err != nil {
		if defaultMetalog == nil {
			return &logrus.TextFormatter{}
		} else {
			return defaultMetalog.configuration.formatter
		}
	}

	switch formatterKey.String() {
	case "json":
		return &logrus.JSONFormatter{}
	default:
		return &logrus.TextFormatter{}
	}
}
