package metalog

import (
	"github.com/sirupsen/logrus"
	"sync"
)

var registry = map[string]*metalog{"DEFAULT": {}}
var registryLock = &sync.RWMutex{}
var registryLoaded = false

func GetLogger(sources ...interface{}) *logrus.Logger {
	registryLock.Lock()
	defer registryLock.Unlock()
	var name = "DEFAULT"
	if len(sources) > 0 && sources[0] != nil {
		name = sources[0].(string)
	}

	if !registryLoaded {
		source := interface{}("metalog.ini")
		if len(sources) > 1 && sources[1] != nil {
			source = sources[1]
		}
		getConfig(source)
		registryLoaded = true
	}

	if requestedLogger, ok := registry[name]; ok {
		if requestedLogger.logger != nil {
			return requestedLogger.logger
		} else {
			logger := logrus.New()
			logger.Level = requestedLogger.configuration.level
			logger.Out = requestedLogger.configuration.output
			logger.Formatter = requestedLogger.configuration.formatter
			registry[name].logger = logger
			return logger
		}
	} else {
		logger := logrus.New()
		logger.WithFields(logrus.Fields{
			"requestedLogger": name,
		}).Warn("GetLogger called with unconfigured logger")
		return logger
	}
}
