package metalog

import (
	"github.com/sirupsen/logrus"
	"os"
)

type metalog struct {
	logger        *logrus.Logger
	configuration loggingConfig
}

type loggingConfig struct {
	level  logrus.Level
	output *os.File
	formatter logrus.Formatter
}

var levelMap = map[string]logrus.Level {
	"panic": logrus.PanicLevel,
	"fatal": logrus.FatalLevel,
	"error": logrus.ErrorLevel,
	"warn":  logrus.WarnLevel,
	"info":  logrus.InfoLevel,
	"debug": logrus.DebugLevel,
	"trace": logrus.TraceLevel,
}
