module gitlab.com/airhorndevelopment/metalog

go 1.15

require (
	github.com/sirupsen/logrus v1.8.1
	gopkg.in/ini.v1 v1.62.0
)
